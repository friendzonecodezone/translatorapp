
import {Component, ElementRef, Renderer} from '@angular/core';
import {Http} from "@angular/http";
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import Speech from 'speak-tts';

@Component({
    selector: 'translator-component',
    templateUrl: './translate.component.html'
})
export class TranslatorComponent {
    public translatedText;
    public selectOption = [
        {
            id: 1,
            label: "Russian",
            value: "ru"
        }, {
            id: 2,
            label: "Spanish",
            value: "es"
        }, {
            id: 3,
            label: "French",
            value: "fr"
        }, {
            id: 4,
            label: "Chinese",
            value: "zh"
        }, {
            id: 5,
            label: "English",
            value: "en"
        }
    ];
    public language;
    constructor(public element: ElementRef, public renderer: Renderer, public http: Http
    ) {
    }

    ngOnInit() {
        this.language = this.selectOption[1].value;
        Speech.init()

    }

    public translateText(text, language) {
        if (text != undefined && language != undefined) {
            var uriText = encodeURIComponent(text);
            if (uriText != "") {
                this.http.get('https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20170329T180255Z.3c18d2dc7b65d525.f23ed9a9efa992bded4ef96334e3c154f61d2dea&lang=' + language + '&text=' + encodeURIComponent(text))
                    .subscribe((response) => {
                        this.translatedText = response.json().text[0];
                    })
            } else {
                this.http.get('https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20170329T180255Z.3c18d2dc7b65d525.f23ed9a9efa992bded4ef96334e3c154f61d2dea&lang=' + language + '&text=%20')
                    .subscribe((response) => {
                        this.translatedText = response.json().text[0];
                    })
            }
        } else {
            this.translatedText = "Nothing to translate...!!";
        }
    }

    public speak() {
        if (this.language == "es") {
            Speech.setLanguage(this.language + '-ES')
            Speech.speak({
                text: this.translatedText,
                onError: (e) => {console.log('sorry an error occured.', e)}, // optionnal error callback
                onEnd: () => {console.log('your text has successfully been spoken.')} // optionnal onEnd callback
            })
        } else if (this.language == "ru") {
            Speech.setLanguage(this.language + '-RU')
            Speech.speak({
                text: this.translatedText,
                onError: (e) => {console.log('sorry an error occured.', e)}, // optionnal error callback
                onEnd: () => {console.log('your text has successfully been spoken.')} // optionnal onEnd callback
            })
        } else if (this.language == "fr") {
            Speech.setLanguage(this.language + '-FR')
            Speech.speak({
                text: this.translatedText,
                onError: (e) => {console.log('sorry an error occured.', e)}, // optionnal error callback
                onEnd: () => {console.log('your text has successfully been spoken.')} // optionnal onEnd callback
            })
        } else if (this.language == "zh") {
            Speech.setLanguage(this.language + '-TW')
            Speech.speak({
                text: this.translatedText,
                onError: (e) => {console.log('sorry an error occured.', e)}, // optionnal error callback
                onEnd: () => {console.log('your text has successfully been spoken.')} // optionnal onEnd callback
            })
        } else {
            Speech.setLanguage(null)
            Speech.speak({
                text: this.translatedText,
                onError: (e) => {console.log('sorry an error occured.', e)}, // optionnal error callback
                onEnd: () => {console.log('your text has successfully been spoken.')} // optionnal onEnd callback
            })
        }
    }
}
