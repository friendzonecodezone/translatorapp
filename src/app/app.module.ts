import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { TranslatorComponent } from './translate-component/translate.component';
import {MdRadioModule} from '@angular/material';
import {MdInputModule} from '@angular/material';
@NgModule({
  declarations: [
    AppComponent,TranslatorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MdRadioModule,
    MdInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
